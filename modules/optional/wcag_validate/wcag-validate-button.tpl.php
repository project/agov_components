<?php

/**
 * @file
 * Provides markup for WCAG button.
 */

?>

<div class="wcag_validate" style="position:fixed; bottom:30px; left:30px;">
  <a href="javascript:void(0);" id="wcag_validate_link"><?php print $title; ?></a>
</div>
