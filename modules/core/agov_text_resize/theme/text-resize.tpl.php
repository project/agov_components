<?php

/**
 * @file
 * Provides template for agov_text_resize.
 */

?>

<div id="resize-buttons">
  <span class="element-invisible">Text size</span>
	<ul>
		<li class="font-small">
			<a href="#">
		    <?php print t('Smaller text'); ?>
			</a>
		</li>
		<li class="font-large">
			<a href="#">
				<?php print t('Larger text'); ?>
			</a>
		</li>
		<li class="reset">
			<a href="#">
				<?php print t('Reset text size'); ?>
			</a>
		</li>
	</ul>
</div>
